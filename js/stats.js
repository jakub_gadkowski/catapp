/**
 * Log touch information in the data
 * @param {boolean} isScored
 */
CA.gameStats = function (isScored) {
    var scoredHit = '';
    if (isScored === true) {
        CA.score.hit += 1;
        CA.scoredHit = '1';
        //used to start hit animation
        CA.lastTouchTarget.hit = 1;
    } else {
        CA.score.escaped += 1;
        scoredHit = '0';
        CA.lastTouchTarget.hit = 0;
    }

    //log touch in the data array and add info about hit/miss
    CA.score.gameData[CA.score.taps] = {
        timeStamp: Date.now(),
        xCoord: CA.touchTarget[0].clientX,
        yCoord: CA.touchTarget[0].clientY,
        mouseXCoord: CA.xPos,
        mouseYCoord: CA.yPos,
        hit: scoredHit
    };
    CA.score.taps += 1;
};

/**
 * Log data every interval to provide statistical data
 */
CA.logData = function (){
    var touchXValue,
        touchYValue;

    //if the last touch point is the same as current - put empty value as there was no new touch.
    //chrome assigns Touch.identifier as 0, bug perhaps. So if identifier is 0 try to check xpos/ypos
    if (CA.touchTarget[0].identifier !== 0) {
        if (CA.lastTouchTarget.identifier === CA.touchTarget[0].identifier) {
            touchXValue = '';
            touchYValue = '';
        } else {
            touchXValue = CA.touchTarget[0].clientX;
            touchYValue = CA.touchTarget[0].clientY;
        }
    } else {//if Touch.identifier is not assigned properly use xpos/ypos
        if (CA.lastTouchTarget.xpos === CA.touchTarget[0].clientX && CA.lastTouchTarget.ypos === CA.touchTarget[0].clientY) {
            touchXValue = '';
            touchYValue = '';
        } else {
            touchXValue = CA.touchTarget[0].clientX;
            touchYValue = CA.touchTarget[0].clientY;
        }
    }

    CA.score.gameData[CA.score.taps] = {
        timeStamp: Date.now(),
        xCoord: touchXValue,
        yCoord: touchYValue,
        mouseXCoord: CA.xPos,
        mouseYCoord: CA.yPos
    };
    CA.score.taps += 1;

    CA.lastTouchTarget.identifier = CA.touchTarget[0].identifier;
    CA.lastTouchTarget.xpos = CA.touchTarget[0].clientX;
    CA.lastTouchTarget.ypos = CA.touchTarget[0].clientY;
};

