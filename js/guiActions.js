


/**
 * Toggle different screens Main screen/Play screen
 */
CA.changeScene = function (){

    if (CA.sceneParts.startMenu.state === 0) {
        CA.sceneParts.startMenu.sceneTarget.style.display = 'none';
    }else{
        CA.sceneParts.startMenu.sceneTarget.style.display = 'block';
        CA.showSummary();
        CA.hlp.printLocalStorage();
    }

    if (CA.sceneParts.play.state === 0) {
        CA.sceneParts.play.sceneTarget.style.display = 'none';
    }else{
        CA.sceneParts.play.sceneTarget.style.display = 'block';
    }
};

/**
 * Handle touch events on different areas of Main/Play screen
 * @param {Object} e - Touch event object
 */
CA.handleTouch = function (e) {

    //prevents initial touch event that cause rubberband effect
    e.preventDefault();
    //log current touch list. This will reset on the next touch.
    CA.touchTarget = e.changedTouches;

    if (e.target.id === 'start_game' || e.target.id === 'restart') {
        CA.playSound(CA.audioArray.sounds.click);
        CA.gameStart();
    } else if (e.target.id === 'end') {
        CA.gameEnd();
    } else if (e.target.id === 'send_results') {
        CA.sendGameResults();
    } else if (e.target.id === 'clear_results') {
        CA.clearGameResults();
    } else if (e.target.id === 'gameArea') {
        CA.handleCanvas();
    } else if (e.target.id === 'sprite_type' || e.target.id === 'movement_type') {
        CA.settingsMenu(e.target);
    }
};

/**
 * Handle touch events on Sprite
 */
CA.handleCanvas = function () {
    //don't do anything until first touch event
    if (CA.delayedStartVal !== 0){

        //Check if sprite was touched
        var result = CA.checkPixelPerfectTwo();

        //if all test are positive hit was registered on the sprite
        if (result === 1){
            console.log('hit');
            CA.gameStats(true);
            var randomSound = CA.pickRandomSound(CA.audioArray.sounds);
            CA.playSound(randomSound);
        //if hit not detected log 'missed' touch
        } else {
            CA.gameStats(false);
        }
    } else {
        CA.delayedStartVal = 1;
    }
};

/**
 * Handle changing button images (text) on Main Screen
 * @param {Object} target - Touch Event target
 */
CA.settingsMenu = function (target){
    //if click was registered on 'Sprite Type'
    if (target.id === 'sprite_type'){
        //if 'Sprite Type' is set to 'Mouse'
        if (CA.settings.sprite === 'mouse'){
            CA.settings.sprite = 'pointer';
            target.className = 'pointer';
            //if 'Sprite Type' is set to 'Pointer'
        }else{
            CA.settings.sprite = 'mouse';
            target.className = 'mouse';
        }
        //if click was registered on 'Movement Type'
    }else{
        if (CA.settings.movement === 'bio'){
            CA.settings.movement = 'non-bio';
            target.className = "non_bio";
            //if 'Sprite Type' is set to 'Pointer'
        }else{
            CA.settings.movement = 'bio';
            target.className = 'bio';
        }
    }
};

/**
 * Show experiment summary on finish or show given text
 * @param {string} text
 */
CA.showSummary = function (text){
    text = text || '';
    var summary = '';
    if (text !== '') {
        summary = text + "<br>\n";
//        console.log('text: '+text);
    }
    var summaryTarget = document.getElementById('end_info');

    for (var i = 0; i < localStorage.length; i++){
        summary += decodeURIComponent(localStorage.key(i))+ "<br>\n";
    }
    //print results
    summaryTarget.innerHTML = summary;
};

/**
 * Deactivate 'send' button and show wait animation on Send data
 * @param {string} status - hide/show
 */
CA.waitAnim = function (status){
    status = status || 'hide';

    var sendButton = document.getElementById('send_results');
    if (status === 'show'){
        sendButton.className = 'wait_anim';
    }else{
        sendButton.className = '';
    }
};