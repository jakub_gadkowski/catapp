/**
 * Reminder to keep tablet always in the same position
 */
CA.handleOrientation = function () {
    if (window.orientation !== 90) {
        var message = "For consistent results please lock your iPad\n in horizontal position with 'Home' button to the right.";
        alert(message);
    }
};

/**
 * Setup Play screen (canvas)
 */
CA.setPlayAreaCanvas = function (){
    CA.sceneH = window.innerHeight;
    CA.sceneW = window.innerWidth;
    CA.yMin = CA.menuHeight;

    var canvasElement = document.createElement('canvas');
    canvasElement.setAttribute('id','gameArea');
    canvasElement.setAttribute('width', CA.sceneW);
    canvasElement.setAttribute('height', CA.sceneH);

    if (canvasElement.getContext){
        CA.canvasCTX = canvasElement.getContext("2d");
        document.getElementById('scene').appendChild(canvasElement);
        CA.createGhostCanvas(CA.sceneW, CA.sceneH);

        //check for existence of sprite on canvas, otherwise we will get bunch of errors
        // if (typeof(sprite) !== 'object') {
        CA.createSprite();
        // }
    } else {
        alert('Canvas not Supported');
    }
};

/**
 * Create ghost Canvas to check for pixelperfect touch events
 * @param {int} width
 * @param {int} height
 */
//TODO create object version of cGC
CA.createGhostCanvas = function (width, height){
    var ghostCanvas = document.createElement('canvas');
    ghostCanvas.width = width;
    ghostCanvas.height = height;
    CA.ghostCTX = ghostCanvas.getContext("2d");
};

/**
 * Resize Play screen.
 * Not really used anymore as the app is locked to iPad screen by pregenerated movement coordinates.
 * Left for test/presentation purposes
 * @deprecated
 */
CA.resizeScene = function (){
    var menu = document.getElementById('menu');
    if (menu){
        var menuH = menu.getBoundingClientRect();
        CA.menuHeight = menuH.height;
    }else {
        CA.menuHeight = 0;
    }

    var scene = document.getElementById("scene");
    var newWidth = window.innerWidth;
    var newHeight = window.innerHeight - CA.menuHeight;
    scene.style.height = newHeight + 'px';
    scene.style.width = newWidth + 'px';
    CA.setPlayAreaCanvas();
};
