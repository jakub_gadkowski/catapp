/*
 * The CatApp (c) 2013-2014 by Jakub Gadkowski/Code Lens
 * http://code-lens.com
 * info@code-lens.com
 */

/*
 * Settings
 */
CA.settings = {
    sprite:'mouse',
    movement:'bio',
    sprites:{
        mouseSprite: {
            url:'images/mouse@1x.png',
            centerX:-175,
            centerY:-30,
            frameWidth:234,
            frameHeight:60,
            anim:[0,0],
            hitAnim:[0,0,0,0]
        },
        pointerSprite:{
            url:'images/white_dot_anim_v2.png',
            centerX:-75,
            centerY:-75,
            frameWidth:150,
            frameHeight:150,
            anim:[0,0],
            hitAnim:[150,0,150,0,150,0,150,0,150,0,150,0,150,0,150,0,150,0,150,0,150,0,150,0,150,0,150,0,150,0,150,0,150,0,150,0,150,0,150,0,150,0,150,0,150,0]
        }
    },
    movementMatrix:{
        bio:{
            url:'data/bio-coords-v4.txt'
        },
        nonBio:{
            url:'data/non-bio-coords-v4.txt'
        }
    },
    getSprite:function(){
        if(this.sprite==='mouse') {
            CA.spriteData.centerX = this.sprites.mouseSprite.centerX;
            CA.spriteData.centerY = this.sprites.mouseSprite.centerY;
            CA.spriteData.width = this.sprites.mouseSprite.frameWidth;
            CA.spriteData.height = this.sprites.mouseSprite.frameHeight;
            CA.spriteData.anim = this.sprites.mouseSprite.anim;
            CA.spriteData.hitAnim = this.sprites.mouseSprite.hitAnim;
            return this.sprites.mouseSprite.url;
        }else{
            CA.spriteData.centerX = this.sprites.pointerSprite.centerX;
            CA.spriteData.centerY = this.sprites.pointerSprite.centerY;
            CA.spriteData.width = this.sprites.pointerSprite.frameWidth;
            CA.spriteData.height = this.sprites.pointerSprite.frameHeight;
            CA.spriteData.anim = this.sprites.pointerSprite.anim;
            CA.spriteData.hitAnim = this.sprites.pointerSprite.hitAnim;
            return this.sprites.pointerSprite.url;
        }
    },
    getMovement:function(){
        if(this.movement==='bio') {
            return this.movementMatrix.bio.url;
        }else{
            return this.movementMatrix.nonBio.url;
        }
    }
};

CA.score = {
    taps: 0,
    hit: 0,
    escaped: 0,
    accuracy: 0,
    gameData: {}
};

CA.lastTouchTarget = {
    identifier: '',
    xpos: '',
    ypos: '',
    hit: ''
};

CA.sceneParts = {
    startMenu : {
        state : 1,
        sceneTarget : (function(){
            return document.getElementById('start_menu');
        }())
    },
    play : {
        state : 0,
        sceneTarget : (function(){
            return document.getElementById('play');
        }())
    }
};

/*
 * Properties
 */
CA.version = '0.9.0';
CA.fps = 25;
CA.interval = 1000 / CA.fps;
CA.animCounter = 0;
CA.menuHeight = 0;
CA.touchTarget = [];
CA.timeoutID = null;
CA.ghostCTX = CA.canvasCTX = null;
CA.yMin = 0;
CA.xPos = CA.yPos = 0;
CA.sceneH = CA.sceneW = 0;
CA.delayedStartVal = 0;
CA.delayStartRAF = null;

/*
 * Initialise
 */

CA.init = function () {
    CA.addListeners();
    CA.changeScene();
    //fillMatrix();
    CA.loadMovementData();
    CA.settings.getSprite();
    CA.initSound();
};


CA.addListeners = function (){
    //window.addEventListener("mousedown", handleTouch, false);
    window.addEventListener("touchstart", CA.handleTouch, false);
    window.addEventListener('orientationchange', CA.handleOrientation, false);
};

/*
 * Main Game Loop
 */

/**
 * Main animation loop.
 * rAF not usable - need to match 25fps of original footage as close as possible
 */
CA.animate = function () {
    CA.timeoutID = setTimeout(function () {
        CA.animate();
        CA.scriptedMotion();
    }, CA.interval);
};

/**
 * Delay start of animation until first touch event
 * @returns void
 */
CA.delayedStart = function () {
    if (CA.delayedStartVal === 0) {
        CA.delayStartRAF = setTimeout(function() {
            CA.delayedStart();
            CA.delayedMotion();

            //Stop Animation after 10 minutes
            CA.animCounter = CA.animCounter + 1;
            if (CA.animCounter >= 18000) {
                clearTimeout(CA.delayStartRAF);
                CA.gameEnd();
            }
        }, CA.interval);
    } else {
        clearTimeout(CA.delayStartRAF);
        CA.matrixPointer = 0;
        CA.animate();
    }
};

CA.gameStart = function (){
    CA.loadMovementData();
    if (CA.getPlayerName() !== false){
        CA.playerUpdates('start');

        CA.sceneParts.startMenu.state = 0;
        CA.sceneParts.play.state = 1;
        CA.changeScene();
        CA.resizeScene();
        CA.playSound(CA.audioArray.sounds.feetNew);
        CA.delayedStart();
    }
};

/*
 * Game End
 */
/**
 * Scene teardown at the end of animation
 */
CA.gameEnd = function (){

    clearTimeout(CA.timeoutID);//stops animation
    CA.playerUpdates('end');
    CA.sceneParts.startMenu.state = 1;
    CA.sceneParts.play.state = 0;
    CA.stopSound();
    CA.changeScene();
    CA.storeGameResults();
    CA.showSummary();
    CA.resetState();
    CA.hlp.printLocalStorage();
};


/**
 * Reset properties between experiments
 */
CA.resetState = function (){
    CA.score = {
        taps: 0,
        hit: 0,
        escaped: 0,
        accuracy: 0,
        gameData:{}
    };

    CA.playerData.reset();

    CA.spriteData = {
        rotation: 0, //rotations of the sprite in radians
        width: 0,
        height: 0,
        centerX: 0,
        centerY: 0,
        currentX: '',
        currentY: '',
        lastX: '',
        lastY: ''
    };

    CA.settings.getSprite();

    CA.xPos = 0;
    CA.yPos = 0;
    CA.matrixPointer = 0;
    //TODO check if this can be used to create new canvas quicker.
    var oldCanvas = document.getElementById('gameArea');
    document.getElementById('scene').removeChild(oldCanvas);
    oldCanvas = null;
    CA.animCounter = 0;
    CA.delayedStartVal = 0;
    CA.matrixPointer = 0;
};