CA.sprite = null;
CA.spriteData = {
    rotation:0, //rotations of the sprite in radians
    width:0,
    height:0,
    centerX:0,
    centerY:0,
    currentX:'',
    currentY:'',
    lastX:'',
    lastY:'',
    anim:[],
    hitAnim:[],
    framePos:0
};
//speed of movement through the movement matrix in ^2
CA.matrixMove = 2;

CA.radians = (Math.PI/180);
/*
 *   Create Sprite
 */

/**
 * Create Image object and load sprite graphics into it
 */
CA.createSprite = function () {
    var loadState = 0;
    CA.sprite = new Image();
    CA.sprite.onload = function() {
        loadState = 1; //on successfull load return 1
    };

    CA.sprite.src = CA.settings.getSprite();
//    return loadState;
};


/*
 *   Draw Sprite
 */

/**
 * Draw given sprite on the Canvas
 * @param {Object} context - canvas object
 * @param {Object} drawnObject - Image object
 * @param {int} rotation - heading in radians
 * @param {int} xPos
 * @param {int} yPos
 * @param {int} xCentre
 * @param {int} yCentre
 * @param {int} sx
 * @param {int} sy
 */
CA.drawSprite = function (context, drawnObject, rotation,xPos,yPos,xCentre,yCentre,sx,sy){
    context.clearRect(0,0,CA.sceneW,CA.sceneH); // clear canvas
    context.save();
    context.translate(xPos,yPos);
    context.rotate(rotation);
    //clear only part that was drawn on
    //CA.canvasCTX.fillRect(xPos, yPos, drawnObject.width, drawnObject.height);
    context.drawImage(drawnObject, sx, sy, CA.spriteData.width, CA.spriteData.height, xCentre,yCentre, CA.spriteData.width, CA.spriteData.height);
//    context.drawImage(drawnObject,xCentre,yCentre);
    context.restore();
};



/*
 *   Sprite Movement
 */

/**
 * Move sprite around Canvas using predefined movement matrix
 */
CA.scriptedMotion = function (){

    //check for x/y coordinates in the movement matrix
    CA.xPos = CA.movementMatrix[CA.matrixPointer];
    CA.yPos = CA.movementMatrix[CA.matrixPointer+1];
//        CA.xPos = 100; //freeze sprite in x - test values
//        CA.yPos = 250; //freeze sprite in y - test values

    //Move sprite around
    var rotation = CA.setSpriteHeading();

    if (CA.lastTouchTarget.hit === 1) {
        var pos = parseInt(CA.spriteData.framePos);
        var arr = CA.spriteData.hitAnim;
        var sx = arr[pos];
        var sy = arr[pos+1];
        CA.drawSprite(CA.canvasCTX,CA.sprite,rotation,CA.xPos,CA.yPos,CA.spriteData.centerX,CA.spriteData.centerY,sx,sy);
        CA.spriteData.framePos = CA.spriteData.framePos + 2;
        var animLength = CA.spriteData.hitAnim.length /2;
        if ( CA.spriteData.framePos >= animLength ){
            CA.lastTouchTarget.hit = 0;
            CA.spriteData.framePos = 0;
        }
    }else {
        CA.drawSprite(CA.canvasCTX,CA.sprite,rotation,CA.xPos,CA.yPos,CA.spriteData.centerX,CA.spriteData.centerY,0,0);
    }

    //move array pointer to the next position or stop the game when pointer reaches the end of the array.
    CA.matrixPointer = CA.matrixPointer + CA.matrixMove;
    if (CA.matrixPointer >= CA.movementMatrix.length) {
        CA.gameEnd();
    }
    CA.logData();
    CA.spriteData.lastX = CA.movementMatrix[CA.matrixPointer-20];
    CA.spriteData.lastY = CA.movementMatrix[CA.matrixPointer-19];
    CA.spriteData.currentX = CA.xPos;
    CA.spriteData.currentY = CA.yPos;
};

/**
 * Animates sprite while waiting for the first touch event.
 * @returns void
 */
CA.delayedMotion = function () {
    //check for x/y coordinates in the movement matrix
    CA.xPos = CA.movementMatrix[CA.matrixPointer];
    CA.yPos = CA.movementMatrix[CA.matrixPointer + 1];

    //Move sprite around
    var rotation = CA.setSpriteHeading();

    CA.drawSprite(CA.canvasCTX, CA.sprite, rotation, CA.xPos, CA.yPos, CA.spriteData.centerX, CA.spriteData.centerY, 0, 0);

    //move array pointer to the next position or stop the game when pointer reaches the end of the array.
    CA.matrixPointer = CA.matrixPointer + CA.matrixMove;
    if (CA.matrixPointer >= CA.movementMatrix.length) {
        CA.matrixPointer = 0;
    }

    CA.spriteData.lastX = CA.movementMatrix[CA.matrixPointer - 20];
    CA.spriteData.lastY = CA.movementMatrix[CA.matrixPointer - 19];
    CA.spriteData.currentX = CA.xPos;
    CA.spriteData.currentY = CA.yPos;
};



/*
*   Sprite Tests
*/

/**
 * Test for sprite collision/hit
 * @returns {int} - returns 1 if collision detected, 0 otherwise
 */
CA.checkPixelPerfectTwo = function (){
    var result = 0;
    //draw sprite on ghost canvas
    CA.drawSprite(CA.ghostCTX, CA.sprite, CA.spriteData.rotation, CA.xPos, CA.yPos, CA.spriteData.centerX, CA.spriteData.centerY,0,0);

    //get touch coordinates within canvas
    var x = CA.touchTarget[0].clientX;
    var y = (CA.touchTarget[0].clientY - CA.menuHeight);

    //create larger touch check
    var lx = x-5;//5*2+1=11
    var ly = y-5;
    //retrieving data at touch coordinates
    var imageData = CA.ghostCTX.getImageData(lx, ly, 11, 11);
    //checking for alpha > 0 if so then register a Hit
    for (var i=0;i<imageData.data.length;i+=4){
        if (imageData.data[i+3] > 0) {
            result = 1;
            break;
        }
    }
    return result;
};

/**
 * Based on previous movement determine sprite heading
 * @returns {int} - returns heading in radians
 */
CA.setSpriteHeading = function (){
    var xData = CA.spriteData.lastX - CA.spriteData.currentX;
    var yData = CA.spriteData.lastY - CA.spriteData.currentY;
    var result;
    // console.log(xData+":"+yData);
    var xChecked = CA.checkHeading(xData);
    var yChecked = CA.checkHeading(yData);
    //console.log(xChecked+":"+yChecked);
    //how much I need to rotate Canvas to give the impression of sprite turning (mouse sprite 'head' is to the left)
    if ( xChecked > 0){//heading left (West)
        switch (yChecked){
            case 1 :
                result = 225; //NW
                break;
            case 0 :
                result = 180; //West
                break;
            case -1 :
                result = 135; //SW
                break;
        }
    }else if (xChecked === 0) { //heading North/South or staying in place
        switch (yChecked){
            case 1 :
                result = 270; //North
                break;
            case 0 : //no change in position
                result = CA.spriteData.rotation*(180/Math.PI); //No change - random or whatever
                break;
            case -1 :
                result = 90; //South
                break;
        }
    } else { //heading right (East)
        switch (yChecked){
            case 1 :
                result = 315; //NE
                break;
            case 0 :
                result = 0; //East
                break;
            case -1 :
                result = 45; //SE
                break;
        }
    }

    //change degrees into radians
    result = result*(CA.radians);
    //first type of rotation smoothing not so good
    //if (Math.abs(result-CA.spriteData.rotation)>=1.57) result = (result+CA.spriteData.rotation)/2;
    //second version of rotation smoothing
    var rotationMod = result - CA.spriteData.rotation;
    if (Math.abs(rotationMod) >= 0.785) {
        var heading = 1;
        if (rotationMod < 0) {
            heading = -1;
        }
        result = CA.spriteData.rotation +((22*(CA.radians))*heading);
    }

    CA.spriteData.rotation = result;
    return result;
};


/**
 * Test for heading change
 * @param {int} value - position on canvas
 * @returns {*} - 1/-1 depending on heading change or 0 if no change
 */
CA.checkHeading = function (value){
    var result;
    if ( value > 0) {
        result = 1;
    }else if (value === 0) {
        result = 0;
    }else {
        result = -1;
    }
    return result;
};

//Not used / need check
//function spriteHeadingAdv(){
//    var alpha,x,y,sqrResult,angle;
//    x = CA.spriteData.lastX - CA.spriteData.currentX;
//    y = CA.spriteData.lastY - CA.spriteData.currentY;
////    x = CA.spriteData.currentX - CA.spriteData.lastX;
////    y = CA.spriteData.currentY - CA.spriteData.lastY;
//
//    sqrResult = Math.sqrt(Math.pow(x,2))+Math.sqrt(Math.pow(y,2));
//    angle = Math.asin(Math.abs(x)/sqrResult);
//   // alpha = angle * 180 / Math.PI;
//
//    if ( x > 0){//heading Left (West)
//        if (y > 0) { //NW
//            angle = angle * Math.PI;
//        }else {//SW
//            angle = Math.PI - angle;
//        }
//    }else {//heading right (East)
//        if (y > 0) { //NE
//            angle = angle - 2*Math.PI;
//        }
//    }
//
//
//    CA.spriteData.rotation = angle;
//    return angle;
//}