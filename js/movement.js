CA.matrixPointer = 0;
CA.movementMatrix = [];

// Load movement coordinates
//TODO Add callback for gamestart
CA.loadMovementData = function () {
    var xhr;
    var url = CA.settings.getMovement(); //TODO CA
    console.log(url);
    xhr = new XMLHttpRequest();

    if (!xhr) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    xhr.onload = function() {
        CA.prepareMovementData(xhr.responseText);
    };
    xhr.open('GET', url);
    xhr.responseType = "text";
    // xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send();
};

CA.prepareMovementData = function (d){
    CA.movementMatrix = [];
    CA.movementMatrix = d.split(',');
    console.log('matrix loaded');
    return 1;
};


//temporary function to fill CA.movementMatrix
CA.fillMatrix = function (){
    var sceneH = window.innerHeight;
    var sceneW = window.innerWidth;
    var a = Math.floor(Math.random() * (sceneH - 0 + 1)) + 0;

    for (var i=0;i<sceneW;i=i+1){
        CA.movementMatrix.push(i,(function(){
            var b = Math.floor(Math.random() * (2 - 1 + 1)) + 1;
            var c = (b>1) ? 1 : -1;
            a=a+(1*c);
            return a;
        }()));
    }
};