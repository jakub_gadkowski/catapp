// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// requestAnimationFrame polyfill by Erik Möller
// fixes from Paul Irish and Tino Zijdel
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x] + 'CancelRequestAnimationFrame'] || window[vendors[x] + 'CancelAnimationFrame'];
    }
 
    if (!window.requestAnimationFrame) {
        window.requestAnimationFrame = function (callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function () {
                    callback(currTime + timeToCall);
                },
                timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
    }
 
    if (!window.cancelAnimationFrame) {
        window.cancelAnimationFrame = function (id) {
            clearTimeout(id);
        };
    }
}());


CA.hlp = {};

CA.hlp.printVersion = function(){
    document.getElementById('version').innerText = 'App version: '+CA.version;
};

CA.hlp.printLocalStorage = function() {
    var countKey, 
        countValue, 
        leftSpace;
    var occupied = 3; //creating localStorage takes 3kb of overhead

    Object.keys(localStorage).forEach(function(key){
            countKey = key;
            countValue = localStorage.getItem(key);
            occupied = occupied + countKey.length + countValue.length;
           });
           
    occupied = (((occupied*16)/(8*1024))/1024).toFixed(2);
    leftSpace = 5 - occupied;
    document.getElementById('version').innerText = 'App version: '+CA.version+" iPad Only | Local Storage space left: "+leftSpace.toFixed(2)+" Mb, ("+ Math.floor(leftSpace/0.7) +" logs left)";
};