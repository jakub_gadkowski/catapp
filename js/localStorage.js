/**
 * Get experiment data from memory, compress it using LZW and store it in localStorage
 */
CA.storeGameResults = function (){
    var userSession = CA.playerData.playerName +"_"+ CA.playerData.startDate;
    var userData = JSON.stringify(CA.score.gameData, null, "\t");

    //try to compress data before storing, to save space - mobile Safari allows only around 5Mb of data in localStorage
    var compressedData = LZW.compress(userData);
    console.log("compressed: "+compressedData.length);
    console.log("uncompressed: "+userData.length);
    try {
        localStorage.setItem(userSession, '"'+compressedData+'"');
    } catch (e) {
        console.log('error: '+ e);
        alert('You have probably run out of local storage space.\n Please save your data and clear local database ("Remove Data" button).');
    }
};

/**
 * Retrieve data from localStorage and prepare it to be send to the server and parsed in PHP
 * @returns {string} - localStorage as JSON object
 */
CA.retrieveGameResults = function (){
    var csvHeaders = 'name,timestamp,x-coord,y-coord,mouse-x-coord,mouse-y-coord,hit';
    var gameResults='{';
    gameResults += '"headers":'+JSON.stringify(csvHeaders)+',';
    gameResults += '"data":{';
    for (var i = 0; i < localStorage.length; i++){
        gameResults += '"'+localStorage.key(i)+'":'+localStorage.getItem(localStorage.key(i));
        if (i+1 < localStorage.length) {
            gameResults += ',';
        }
    }
    gameResults += '}}';

    return gameResults;
};


/**
 * Clear local storage of any results
 */
CA.clearGameResults = function (){
    var message = "Are you sure you want to delete all logged results?";
    var message2 = "Are you really, really sure you want to delete all logged data?";
    var message3 = "Local results database has been cleared.";
    var result = window.confirm(message);
    if (result) {
        var result2 = window.confirm(message2);
        if(result2) {
            localStorage.clear();
            CA.showSummary(message3);
            CA.hlp.printLocalStorage();
        }
    }
};