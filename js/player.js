CA.playerData = {
    playerName: '',
    setPlayerName: function(val){
        this.playerName = encodeURIComponent(val);
    },
    getPlayerName: function(){
        return decodeURIComponent(this.playerName);
    },
    startDate:'',
    endDate:'',
    datePlayed:'',
    timePlayed: '',
    reset: function(){
        this.playerName ='';
        this.startDate ='';
        this.endDate ='';
        this.timePlayed ='';
    }
};

/**
 * Create name for test subject, either using provided name or generating timestamp
 * @returns {boolean} false - on Null prompt (canceled prompt)
 */
CA.getPlayerName = function (){
    var name = CA.promptUser();

    if (name !== '' && name !== null){
        CA.playerData.setPlayerName(name + '_' + CA.settings.sprite + '_' + CA.settings.movement);
    } else if (name === '') { //generate name when dialog left empty
        //console.log('fired');
        var dateNow = new Date();
        var dateFormatted = dateNow.toString();
        var generatedName = 'Player_' + dateFormatted.split(' ').join('_');
        CA.playerData.setPlayerName(generatedName + '_' + CA.settings.sprite + '_' + CA.settings.movement);
    } else { //return to the homescreen when dialog cancelled
        return false;
    }
};

CA.promptUser = function(){
    return window.prompt('Please enter player name (if none given it will be generated):');
};

/**
 * Generate date and start/end time of the experiment
 * @param {string} val - start/end
 */
CA.playerUpdates = function (val){
    var timeNow = Date.now();
    var dateNow = new Date();
    var dateFormatted = dateNow.toLocaleDateString();

    if (val === 'start'){
        CA.playerData.startDate = timeNow;
        CA.playerData.datePlayed = dateFormatted;
    }else if (val === 'end'){
        CA.playerData.endDate = timeNow;
    }
};