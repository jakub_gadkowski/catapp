CA.RESULTS_URL = "php/save.php";

CA.sendGameResults = function () {
    var xhr;
    var url = CA.RESULTS_URL;
    var payload = CA.retrieveGameResults();
    xhr = new XMLHttpRequest();

    if (!xhr) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    xhr.onreadystatechange = CA.handleSendResponse;
    xhr.open('POST', url);
//    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
//   xhr.setRequestHeader('Content-length', payload.length);
//    xhr.setRequestHeader("X-File-Name", 'test');
//    xhr.setRequestHeader("X-File-Size", payload.length);
//    xhr.setRequestHeader("X-File-Type", 'json');
    xhr.send(payload);

};

CA.handleSendResponse = function () {
    try {
        if (this.readyState === 4) {
            CA.waitAnim('hide');
            if (this.status === 200) {
                // var response = JSON.parse(this.responseText);
                var response = this.responseText;
                alert(response);
            }else if (this.status === 0) {
                // var response = JSON.parse(this.responseText);
                alert('There was a problem connecting to the internet.\n Please check if you are connected to the internet and try again.');
            } else {
                alert('There was a problem with the request. Server is down or on fire. Please contact developer and give this server status code'+this.status);
            }
        }else { // sending payload
            CA.waitAnim('show');
        }
    }
    catch (e) {
        alert('Caught Exception: ' + e);
    }
};
