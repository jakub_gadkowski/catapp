CA.audioCTX = null;
CA.audioUnlocked = false;
CA.loopedSound = [];

/**
 * Initialise AudioContext, create raw versions of all sounds for quick acccess
 */
CA.initSound = function (){
    if ('webkitAudioContext' in window || 'AudioContext' in window) {
        CA.audioCTX = new webkitAudioContext() || AudioContext();

        // if (CA.audioUnlocked === false) CA.unlockSound();
        for (var value in CA.audioArray.sounds) {
            var singleSound = CA.audioArray.sounds[value];
            CA.loadSoundCached(singleSound.cache, singleSound);
        }
    } else {
        alert('This device/browser does not support WebAudio API. Please check if you are running it on an iPad');
    }
};

/**
 * problem on iOS6 - sound is not cached when in off line mode
 * @deprecated
 * @param url
 * @param object
 */
CA.loadSound = function(url, object) {
    var requestSound = new XMLHttpRequest();
    requestSound.open('GET', url, true);
    requestSound.responseType = 'arraybuffer';

    // Decode asynchronously
    requestSound.onload = function() {
        CA.audioCTX.decodeAudioData(requestSound.response, function(buffer) {
            object.buffer = buffer;
        });
    };
    requestSound.send();
};

/**
 * Workaround for iOS not caching sounds
 * Loading sound files form soundCache.js as Base64 encoded strings and saving AudioContext raw data for quick reference
 * @param {string} cache - sound encoded as Base64 string
 * @param {Object} object - sound object literal
 */
CA.loadSoundCached = function(cache, object){
    // base64decode the binary audio data into an ArrayBuffer
    var requestSound = Base64Binary.decodeArrayBuffer(cache);
    //decode ArrayBuffer into AudioData
    CA.audioCTX.decodeAudioData(requestSound, function(buffer) {
        //insert Raw Audio Data back into object for quick reuse.
        object.buffer = buffer;
    });
};


/**
 * Play given sound
 * @param {Object} soundObject
 */
CA.playSound = function (soundObject) {
    var source = CA.audioCTX.createBufferSource(); // creates a sound source
    source.buffer = soundObject.buffer;                    // tell the source which sound to play
    source.connect(CA.audioCTX.destination);       // connect the source to the context's destination (the speakers)
    if (soundObject.loop === 1) source.loop = true;
    source.noteOn(0);
    if (soundObject.loop === 1) CA.loopedSound.push(source);
};


/**
 *  Stop playing looped sounds. Sounds are located in an array in FILO order.
 * @param {integer} index - Index of the sound that will be turned off. If no 'index' given turn off all looped sounds.
 * @returns void
 */
CA.stopSound = function (index){
    index = index || '';
    if (index === '') {
        CA.loopedSound.forEach(function(v,i){
            var source = CA.loopedSound[i];
            source.noteOff(0);
        });
    } else {
        var source = CA.loopedSound[index];
        source.noteOff(0);
    }
};

/**
 * Pick random sound from list of sounds, use modifier to restrict range.
 * @param {Object} sounds - list of sounds as object literal
 * @returns {Object} - single sound object
 */
CA.pickRandomSound = function (sounds) {
    var modifier;
    modifier = CA.audioArray.randomModifier;
    var keys = Object.keys(sounds);
    var restriction = (keys.length-modifier);
    // (length - modifier) because we just need two first sounds from the array
    return sounds[keys[Math.floor( restriction * Math.random() )]];
};

//needed for iOS devices where the sound API is locked by default until user interacts.
//This happens only in Safari, in WebView mode sound is unlocked.
//thanx to Paul Bakaus http://paulbakaus.com/
//CA.unlockSound = function () {
//    var myContext = new webkitAudioContext();
//    // create empty buffer and play it
//    var buffer = myContext.createBuffer(1, 1, 22050);
//    var source = myContext.createBufferSource();
//    source.buffer = buffer;
//    source.connect(myContext.destination);
//    source.noteOn(0);
//
//    // by checking the play state after some time, we know if we're really unlocked
//    setTimeout(function() {
//        if ((source.playbackState === source.PLAYING_STATE || source.playbackState === source.FINISHED_STATE)) {
//            CA.audioUnlocked = true;
//        } else {
//            CA.audioUnlocked = false;
//        }
//    }, 0);
//};
