<?php
download_archive();

/* creates a compressed zip file 
 * By Dave Walsh
 */
function create_zip($files = array(),$destination = '',$overwrite = false) {
	//if the zip file already exists and overwrite is false, return false
	if(file_exists($destination) && !$overwrite) { return false; }
	//vars
	$valid_files = array();
	//if files were passed in...
	if(is_array($files)) {
		//cycle through each file
		foreach($files as $file) {
			//make sure the file exists
			if(file_exists($file)) {
				$valid_files[] = $file;
			}
		}
	}
	//if we have good files...
	if(count($valid_files)) {
		//create the archive
		$zip = new ZipArchive();
		if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
			return false;
		}
		//add the files
		foreach($valid_files as $file) {
			$zip->addFile($file,$file);
		}
		//debug
		//echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;
		
		//close the zip -- done!
		$zip->close();
		
		//check to make sure the file exists
		return file_exists($destination);
	}
	else
	{
		return false;
	}
}

//if (isset($_GET['dl'])){

//}

function download_archive(){
    $output = array();
    if(!empty($_GET['dl'])) {
        $output[0] = htmlentities($_GET['dl']);
        $zip_name = str_ireplace('.csv', '', $output[0]) .'.zip';
    } else {
        $directory = './';
        $file_array = scandir($directory);
          if (!empty($file_array)){
            foreach($file_array as $key => $single_file){
                $current_file = pathinfo($single_file);
                if ($current_file['extension'] == 'csv'){

                    array_push($output, $single_file);
                }
            }
            if (empty($output)) exit();
            $zip_name = 'All_data_'. date("Y-m-d-H-i-s") .'.zip';
        }
    }
    
    $result = create_zip($output, $zip_name);
    header('Content-type: application/zip');
    header('Content-Disposition: attachment; filename="'.$zip_name.'"');
    header('Content-Length: '.filesize($zip_name));
    header('Connection: close');
    readfile($zip_name);
    unlink($zip_name);
}
?>