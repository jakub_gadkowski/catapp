<?php
$directory = './';
$file_array = scandir($directory);
$output = '';
date_default_timezone_set('Europe/Amsterdam'); 

if (!empty($file_array)){
    foreach($file_array as $key => $single_file){
        $current_file = pathinfo($single_file);
        if ($current_file['extension'] == 'csv'){
            $file_size = round(filesize($single_file)/1024, 0);
            $file_mod_date = date("Y-m-d H:i:s", filemtime($single_file));
            $output .= '<tr>'."\r\n";
            $output .= '<td>'.$single_file.'</td><td>'.$file_size.'Kb </td><td>'.$file_mod_date.'</td><td><a href="download.php?dl='.$current_file['basename'].'" target="_blank" class="download_button_small">&DownArrowBar;</a></td>'."\r\n";
            $output .= '</tr>'."\r\n";
        }
    }
    if (empty($output)) $output = "No data files to download.";
}


?>
<!DOCTYPE html>
<html>
<head>
 	<!-- fill iPhone screen with canvas -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />

	<title>Download Results</title>
	
        <!-- Load CSS before JS -->
	<link rel="stylesheet" href="../css/style.css" type="text/css">

	<script type='text/javascript'>
	// load JS after page is loaded

	</script>
</head>
<body>
    <!-- Start Screen -->
    <div id='download_menu'>
        <div id='download_items'>
            <table>
                <tr>
                    <th>Name</th><th>File Size</th><th>Last Modified</th><th>Download single</th>
                </tr>
                <?php
                echo $output;
                
                ?>
            </table>
            <a href="download.php?dl=1" target="_blank" class="download_button">Download All </a>
        </div>
    </div>
</body>
</html>