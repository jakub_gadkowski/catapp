<?php
//load compressor/decompressor
require_once 'lzw.php';

function receiveGameData(){
    //simple security check 
    //TODO add simple encrypt/decrypt authentication
//    if ($_POST['data5436773']){
        $data = file_get_contents('php://input');
        $decoded = json_decode($data, true);
        //checking if JSON decode went fine
        $json_check = checkJSONError();
        if (empty($json_check)) {
            //create csv from JSON and save it to file
            $csv_result = generateCSV($decoded); 
            return $csv_result;
        }else{
            return $json_check;
        }
//    }else {
//        return "Sorry, no authentication";
//    }
};

/**
 * generateCSV takes Array converted from JSON string
 * @param string $data String passed by XMLHttpRequest. Contains two 
 * main branches: 'Headers' - simple string of names of columns separated with cpmmas, 
 * and 'Data' - the data that will be a body of CSV.
 * @return string Returns status of the save operation as a message string.
 */
function generateCSV($data){
    //create dynamically generated header from passed JSON string
    $csvHeaders = explode(",", htmlentities($data['headers']));
    $csvHeaders = '"'.implode('","', $csvHeaders).'"';
    $dataOut = '';
    //iterate through player array
    foreach ($data['data'] as $user => $gameData) {
        //decompress data sent from client
        $archiver = new LZW();
        $decompressed_data = $archiver->decompress($gameData);
        //decode JSON 
        $decoded_data = json_decode($decompressed_data);
        //iterate through player's game data
        foreach ($decoded_data as $loggedData) {
            //create csv line
            $combinedData = '';
            foreach ($loggedData as $singleData){
                $combinedData .= ',"'.$singleData.'"';
            }
            $dataOut .= '"'.$user.'"'.$combinedData."\n\r";
        }
    }
    //create final string that will be written to the file
    $out = $csvHeaders."\n\r".$dataOut;
    //generate file name with timestamp
    $file_name = "../saved_data/data_".time().".csv";
    $result = file_put_contents($file_name, $out);
    
    if ($result !== false){
        $status = "Everything fine, data saved.";
    } else {
      $status = "Something went wrong while writing the file";  
    }
    
    return $status;
}

function checkJSONError(){
    switch (json_last_error()) {
            case JSON_ERROR_NONE:
                return '';
            break;
            case JSON_ERROR_DEPTH:
                return ' - Maximum stack depth exceeded';
            break;
            case JSON_ERROR_STATE_MISMATCH:
                return ' - Underflow or the modes mismatch';
            break;
            case JSON_ERROR_CTRL_CHAR:
                return ' - Unexpected control character found';
            break;
            case JSON_ERROR_SYNTAX:
                return ' - Syntax error, malformed JSON';
            break;
            case JSON_ERROR_UTF8:
                return ' - Malformed UTF-8 characters, possibly incorrectly encoded';
            break;
            default:
                return ' - Unknown error';
            break;
        }
}

$response = receiveGameData();
//$response = var_dump(file_get_contents('php://input'));
echo $response;

?>