    describe('Helpers tests',function(){
        
        describe('Test printing out App version', function(){
            beforeEach(function() {
                var element = document.createElement('div');
                element.setAttribute('id','version');
                document.body.appendChild(element);
            });
            
            afterEach(function() {
                var element = document.getElementById('version');
                element.parentNode.removeChild(element);
            });
           it('should print the version number', function(){
               CA.hlp.printVersion();
               expect(document.getElementById('version').innerText).toBeTruthy();
           });
           
           it('should print the version number and localStorage info', function(){
               CA.hlp.printLocalStorage();
               expect(document.getElementById('version').innerText).toBeTruthy();
           }); 
        });
    });

    describe('Movement tests',function(){
        describe('Prepare movement matrix',function(){
            var d = '357,205,349,210';
            var result = CA.prepareMovementData(d);

            it('should split movement matrix into Array', function(){
                expect(CA.movementMatrix).toContain('205');
            });

            it('and should return 1', function(){
                expect(result).toEqual(1);
            });
        });

        describe("Fetch data using XMLHttpRequest", function() {

            beforeEach(function () {
                spyOn(XMLHttpRequest.prototype, 'open').andCallThrough();
                spyOn(XMLHttpRequest.prototype, 'send');
                CA.loadMovementData();
            });

            it("should call XHR open", function() {
                expect(XMLHttpRequest.prototype.open).toHaveBeenCalled();
            });

            it("should call XHR send", function () {
                expect(XMLHttpRequest.prototype.send).toHaveBeenCalled();
            });
        });

    });

    describe('Sound tests',function(){

    });

    describe('Player tests',function(){

        describe('Get Test subject name',function(){

            it("should return false on prompt 'cancel'", function () {
                spyOn(CA,'promptUser').andReturn(null);
                CA.getPlayerName();
                expect(CA.playerData.playerName).toBe('');
            });

            it("should change test subject name to 'Johnny'", function () {
                spyOn(CA,'promptUser').andReturn('Johnny');
                CA.getPlayerName();
                expect(CA.playerData.playerName).toMatch('Johnny');
            });

            it("should create default test subject name", function () {
                spyOn(CA,'promptUser').andReturn('');
                CA.getPlayerName();
                expect(CA.playerData.playerName).toMatch('Player_');
            });

        });

        describe('Update time',function(){

            it("should update Start time and experiment date", function () {
                var test = 'start';
                CA.playerUpdates(test);
                expect(CA.playerData.startDate).not.toBe('');
                expect(CA.playerData.datePlayed).not.toBe('');
            });

            it("should update End time", function () {
                var test = 'end';
                CA.playerUpdates(test);
                expect(CA.playerData.endDate).not.toBe('');
            });
        });

    });

    describe('LocalStorage tests',function(){

//        CA.playerData.playerName = 'Toby';
//        CA.playerData.startDate = Date.now();


        var localKey = 'Test Key';
        var localData = 'Test Data';
        var result;
        var timestamp = Date.now();

        beforeEach(function () {
            spyOn(CA.playerData,'playerName').andReturn('Toby');
            spyOn(CA.playerData,'startDate').andReturn(timestamp);
            CA.score.gameData[0] = {
                timeStamp: timestamp,
                xCoord: 111,
                yCoord: 222,
                mouseXCoord: 333,
                mouseYCoord: 444,
                hit: 1
            };
            result = null;
            localStorage.clear();
        });

        afterEach(function () {
            CA.score.gameData = {};
            localStorage.clear();
        });

        it("should compress and save experiment data into LocalStorage", function () {
            CA.storeGameResults();

            result = localStorage.getItem(CA.playerData.playerName +"_"+ CA.playerData.startDate);
            console.log(result);
            expect(result).toBeDefined();
        });

        it("should retrieve experiment data from LocalStorage", function () {
            localStorage.setItem(localKey, localData);
            result = CA.retrieveGameResults();
            expect(result).toMatch('Test Key');
        });

    });