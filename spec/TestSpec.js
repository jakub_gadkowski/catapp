/*
 * Test test
 */

describe('Player tests',function(){

    describe('Get Test subject name',function(){

        it("should return false on prompt 'cancel'", function () {
            spyOn(CA,'promptUser').andReturn(null);
            CA.getPlayerName();
            expect(CA.playerData.playerName).toBe('');
        });

        it("should change test subject name to 'Johnny'", function () {
            spyOn(CA,'promptUser').andReturn('Johnny');
            CA.getPlayerName();
            expect(CA.playerData.playerName).toMatch('Johnny');
        });

        it("should create default test subject name", function () {
            spyOn(CA,'promptUser').andReturn('');
            CA.getPlayerName();
            expect(CA.playerData.playerName).toMatch('Player_');
        });

    });

    describe('Update time',function(){

        it("should update Start time and experiment date", function () {
            var test = 'start';
            CA.playerUpdates(test);
            expect(CA.playerData.startDate).not.toBe('');
            expect(CA.playerData.datePlayed).not.toBe('');
        });

        it("should update End time", function () {
            var test = 'end';
            CA.playerUpdates(test);
            expect(CA.playerData.endDate).not.toBe('');
        });
    });

});